import UnityEngine

class MoveCameraScript (MonoBehaviour): 

	cameras as List = ["CameraAtriumSouth", "CameraAtriumEast", "CameraAtriumNorth", "CameraAtriumWest"];
	currentCamera = 0;

	def Start ():
		pass
		
	def Update ():
		for cam in Camera.allCameras:
			cam.enabled = false;
		if (Input.GetKeyDown(KeyCode.Space)):
			currentCamera += 1;
			if (currentCamera >= cameras.Count):
				currentCamera = 0;
		GameObject.Find(cameras[currentCamera]).camera.enabled = true;
		
